//
//  ViewController.swift
//  ForceDirectedGraph
//
//  Created by Francisco Javier Delgado García on 08/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

extension UIColor {
    fileprivate convenience init(rgbaValue: UInt32) {
        let max = CGFloat(UInt8.max)
        self.init(red: CGFloat((rgbaValue >> 24) & 0xFF) / max,
                  green: CGFloat((rgbaValue >> 16) & 0xFF) / max,
                  blue: CGFloat((rgbaValue >> 8) & 0xFF) / max,
                  alpha: CGFloat(rgbaValue & 0xFF) / max)
    }
}

class ViewController: UIViewController {
    
    private let center: Center<ViewParticle> = Center(.zero)
    
    private let manyParticle: ManyParticle<ViewParticle> = ManyParticle()
    
    private let links: Links<ViewParticle> = Links()
    
    private lazy var linkLayer: CAShapeLayer = {
        let linkLayer = CAShapeLayer()
        linkLayer.strokeColor = UIColor.gray.cgColor
        linkLayer.fillColor = UIColor.clear.cgColor
        linkLayer.lineWidth = 2
        self.view.layer.insertSublayer(linkLayer, at: 0)
        return linkLayer
    }()
    
    fileprivate lazy var simulation: Simulation<ViewParticle> = {
        let simulation: Simulation<ViewParticle>  = Simulation()
        simulation.insert(force: self.manyParticle)
        simulation.insert(force: self.links)
        simulation.insert(force: self.center)
        simulation.insert(tick: { self.linkLayer.path = self.links.path(from: &$0) })
        return simulation
    }()
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        
        func particle() -> ViewParticle {
            let view = UIView()
            view.center = CGPoint(x: CGFloat(arc4random_uniform(320)), y: -CGFloat(arc4random_uniform(100)))
            view.bounds = CGRect(x: 0, y: 0, width: 50, height: 50)
            self.view.addSubview(view)
            
            let gestureRecogizer = UIPanGestureRecognizer(target: self, action: #selector(dragged(_:)))
            view.addGestureRecognizer(gestureRecogizer)
            
            let imageLayer = CALayer()
            imageLayer.backgroundColor = UIColor.clear.cgColor
            imageLayer.bounds = CGRect(x: 0, y: 0 , width: 40, height: 60)
            imageLayer.position = CGPoint(x: 25, y: 10)
            imageLayer.contents = UIImage(named: "face")?.cgImage
            imageLayer.cornerRadius = 5
            imageLayer.masksToBounds = true
            view.layer.addSublayer(imageLayer)
            
            let particle = ViewParticle(view: view)
            simulation.insert(particle: particle)
            return particle
        }
        
        let a = particle()
        let b = particle()
        let c = particle()
        let d = particle()
        let e = particle()
        let f = particle()
        
        let g = particle()
        let h = particle()
        let i = particle()
        let j = particle()
        let k = particle()
        let l = particle()
        
        
        links.link(between: a, and: b, distance: 100)
        links.link(between: a, and: c, distance: 100)
        links.link(between: c, and: b, distance: 40)
        links.link(between: d, and: b, distance: 100)
        links.link(between: e, and: c, distance: 100)
        links.link(between: e, and: f, distance: 100)
        links.link(between: f, and: g, distance: 70)
        links.link(between: f, and: h, distance: 70)
        links.link(between: f, and: i, distance: 70)
        links.link(between: f, and: j, distance: 70)
        links.link(between: f, and: k, distance: 70)
        links.link(between: f, and: l, distance: 70)
    }
    
    override func viewWillLayoutSubviews() {
        linkLayer.frame = view.bounds
        center.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        simulation.start()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        simulation.stop()
    }
    
    @objc private func dragged(_ gestureRecognizer: UIPanGestureRecognizer) {
        guard let view = gestureRecognizer.view, let index = simulation.particles.firstIndex(of: ViewParticle(view: view)) else { return }
        var particle = simulation.particles[index]
        switch gestureRecognizer.state {
        case .began:
            particle.fixed = true
        case .changed:
            particle.position = gestureRecognizer.location(in: self.view)
            simulation.kick()
        case .cancelled, .ended:
            particle.fixed = false
            particle.velocity += gestureRecognizer.velocity(in: self.view) * 0.05
        default:
            break
        }
        simulation.particles.update(with: particle)
    }
}

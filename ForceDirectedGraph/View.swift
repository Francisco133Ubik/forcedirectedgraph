//
//  View.swift
//  ForceDirectedGraph
//
//  Created by Francisco Javier Delgado García on 08/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//


import UIKit

public struct ViewParticle: Particle {
    public var velocity: CGPoint
    public var position: CGPoint
    public var fixed: Bool
    fileprivate let view: Unmanaged<UIView>
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(view.takeUnretainedValue().hashValue)
    }
    
    public init(view: UIView) {
        self.view = .passUnretained(view)
        self.velocity = .zero
        self.position = view.center
        self.fixed = false
    }
    
    @inline(__always)
    public func tick() {
        view.takeUnretainedValue().center = position
    }
}

public func ==(lhs: ViewParticle, rhs: ViewParticle) -> Bool {
    return lhs.view.toOpaque() == rhs.view.toOpaque()
}
